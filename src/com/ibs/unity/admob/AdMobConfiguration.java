package com.ibs.unity.admob;

import android.view.Gravity;
import android.widget.LinearLayout;

import com.google.ads.AdSize;

class AdMobConfiguration {

	final String publisherId;
	final AdSize size;
	final int orientation;
	final int gravity;

	AdMobConfiguration(String publisherId, int size, int orientation, int horizontalPosition, int verticalPosition) {
		this.publisherId = publisherId;
		this.size = AdMobConfiguration.parseAdSize(size);
		this.orientation = AdMobConfiguration.parseOrientation(orientation);
		this.gravity = AdMobConfiguration.parseGravity(horizontalPosition, verticalPosition);
	}

	private static AdSize parseAdSize(int size) {

		switch (size) {
			case 0:
				return (AdSize.BANNER);
			case 1:
				return (AdSize.IAB_MRECT);
			case 2:
				return (AdSize.IAB_BANNER);
			case 3:
				return (AdSize.IAB_LEADERBOARD);
			case 4:
				return (AdSize.SMART_BANNER);
		}

		return (AdSize.BANNER);
	}

	private static int parseOrientation(int orientation) {

		switch (orientation) {
		case 0:
			return (LinearLayout.HORIZONTAL);
		case 1:
			return (LinearLayout.VERTICAL);
		}

		return (LinearLayout.HORIZONTAL);
	}

	private static int parseGravity(int horizontalPosition, int verticalPosition) {

		int gravity = Gravity.NO_GRAVITY;

		switch (horizontalPosition) {
			case 0:
				gravity = Gravity.CENTER_HORIZONTAL;
				break;
			case 1:
				gravity = Gravity.LEFT;
				break;
			case 2:
				gravity = Gravity.RIGHT;
				break;
			default:
				gravity = Gravity.CENTER_HORIZONTAL;
		}

		switch (verticalPosition) {
			case 0:
				gravity |= Gravity.CENTER_VERTICAL;
				break;	
			case 1:
				gravity |= Gravity.TOP;
				break;	
			case 2:
				gravity |= Gravity.BOTTOM;
				break;
			default:
				gravity = Gravity.BOTTOM;
		}

		return (gravity);
	}

	@Override
	public String toString() {
		String tmp;

		// Work around SMART_BANNER bug
		try {
			tmp = this.size.toString();
		} catch (Exception error) {
			tmp = (this.size == AdSize.SMART_BANNER ? "SMART_BANNER" : "???");
		}

		return ("AdMobConfiguration{"
				+ "publisherId: " + this.publisherId
				+ ", "
				+ "size: " + tmp + ", " 
				+ "orientation: " + this.orientation 
				+ ", " 
				+ "gravity: " + this.gravity + "}");
	}
	
	public int getAdWidth(){		
		return size.getWidth();
	}
	
	public int getAdHeight(){
		return size.getHeight();
	}
}
