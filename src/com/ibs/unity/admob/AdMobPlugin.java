package com.ibs.unity.admob;


import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.google.ads.Ad;
import com.google.ads.AdListener;
import com.google.ads.AdRequest;
import com.google.ads.AdRequest.ErrorCode;
import com.google.ads.AdView;
import com.unity3d.player.UnityPlayer;


public class AdMobPlugin{
	static String LOGTAG = "AdMobPlugin";

	private static final AdMobPlugin instance = new AdMobPlugin();

	private Activity activity;
	private AdMobConfiguration config;
	private LinearLayout layout;
	private AdView view;
	private int received;
	private ErrorCode lastError;

	private Runnable CONF = new Runnable(){ @Override public void run(){ _conf(); } };
	private Runnable SHOW = new Runnable(){ @Override public void run(){ _show(); } };
	private Runnable HIDE = new Runnable(){ @Override public void run(){ _hide(); } };
	private Runnable LOAD = new Runnable(){ @Override public void run(){ _load(); } };
	
	private AdListener AD_LISTENER = new AdListener(){

		@Override
		public void onReceiveAd(Ad ad){
			received++;
		}

		@Override
		public void onFailedToReceiveAd(Ad ad, ErrorCode errorCode){
			lastError = errorCode;
		}

		@Override
		public void onPresentScreen(Ad ad){
		}

		@Override
		public void onDismissScreen(Ad ad){
		}

		@Override
		public void onLeaveApplication(Ad ad){
		}
	};

	public static AdMobPlugin getInstance(
		String publisherId,
		int size,
		int orientation,
		int horizontalPosition,
		int verticalPosition
	){

		if(AdMobPlugin.instance.config == null){

			AdMobPlugin.instance.activity = UnityPlayer.currentActivity;
			AdMobPlugin.instance.config = new AdMobConfiguration(publisherId, size, orientation, horizontalPosition, verticalPosition);

			AdMobPlugin.instance.activity.runOnUiThread(AdMobPlugin.instance.CONF);
		}

		return(AdMobPlugin.instance);
	}

	private AdMobPlugin(){
	}

	public void reconfigure(
		String publisherId,
		int size,
		int orientation,
		int horizontalPosition,
		int verticalPosition
	){

		this.config = new AdMobConfiguration(publisherId, size, orientation, horizontalPosition, verticalPosition);

		this.activity.runOnUiThread(this.CONF);
	}

	private void _conf(){
		boolean uninitialized = (this.layout == null);

		try{
			if(uninitialized){
				this.layout		= new LinearLayout(this.activity);
			}

			if(this.view != null){
				this.layout.removeViewInLayout(this.view);
			}

			this.layout.setOrientation(config.orientation);
			this.layout.setGravity(config.gravity);

			this.view = new AdView(activity, config.size, config.publisherId);

			this.view.setAdListener(this.AD_LISTENER);

			// Add the view to the layout
			this.layout.addView(this.view);

			if(uninitialized){
				this.activity.addContentView( this.layout, new LayoutParams(-1, -1) );
			}
		}catch(Exception error){
			error.printStackTrace();
		}
	}

	public void show(){
		this.activity.runOnUiThread(this.SHOW);
	}

	private void _show(){
		try{
			this.view.setVisibility(View.VISIBLE);
		}catch(Exception error){
			error.printStackTrace();
		}
	}

	public void hide(){
		this.activity.runOnUiThread(this.HIDE);
	}

	private void _hide(){
		try{
			this.view.setVisibility(View.GONE);
		}catch(Exception error){
			error.printStackTrace();
		}
	}

	public void load(){
		this.activity.runOnUiThread(this.LOAD);
	}

	private void _load(){
		try{
			// Create the ad request
			AdRequest adRequest = new AdRequest();
			
			// Load the ad
			this.view.loadAd(adRequest);
		}catch(Exception error){
			error.printStackTrace();
		}
	}

	public String getLastError(){
		String error;

		if(this.lastError == null){
			return(null);
		}

		error = this.lastError.toString();

		this.lastError = null;

		return(error);
	}

	public int getReceived(){
		
		return(this.received);
	}
	
	public int getWidth(){
		return config.getAdWidth();
	}
	
	public int getHeight(){
		return config.getAdHeight();
	}	
}
